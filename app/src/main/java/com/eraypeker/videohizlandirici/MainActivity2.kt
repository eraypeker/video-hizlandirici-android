package com.eraypeker.videohizlandirici

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val hizlan = findViewById<Button>(R.id.hizlan)
        val hiz = findViewById<TextView>(R.id.hiz) as EditText?
        val sifirla = findViewById<Button>(R.id.sifirla)
        val bundle :Bundle ?=intent.extras
        val link3 = bundle?.getString("link3")

        val webView: WebView = findViewById(R.id.webView)

        webView.webViewClient = WebViewClient()
        webView.webChromeClient = WebChromeClient()
        webView.loadUrl(link3.toString())
        webView.settings.javaScriptEnabled = true
        webView.settings.setSupportZoom(true)

        fun speed(hiz2: String){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                webView.evaluateJavascript("javascript:(function(){document.getElementsByTagName(\"video\")[0].playbackRate=${hiz2};})();", null)
            } else {
                webView.loadUrl("javascript:(function(){document.getElementsByTagName(\"video\")[0].playbackRate=${hiz2};})();")
            }
        }

        hizlan?.setOnClickListener() {
            Toast.makeText(this, "._.", Toast.LENGTH_SHORT).show()
            speed(hiz?.text.toString())
        }

        sifirla.setOnClickListener() {
            Toast.makeText(this, "._.", Toast.LENGTH_SHORT).show()
            speed("1")
        }

    }

    override fun onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack()
        } else {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

    }
}