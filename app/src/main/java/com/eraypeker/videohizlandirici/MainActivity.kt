package com.eraypeker.videohizlandirici

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.io.File

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button1: Button = findViewById(R.id.button1)
        val link = findViewById<TextView>(R.id.text1) as EditText
        val http = "https://"
        var link2: String

        button1.setOnClickListener(){
            val intent = Intent(this, MainActivity2::class.java)

            if (!link.text.startsWith("http://") && !link.text.startsWith("https://")) {
                link2 = http + link.text.toString()
            } else {
                link2 = link.text.toString()
            }

            intent.putExtra("link3", link2)

            Toast.makeText(this, "Link yükleniyor...", Toast.LENGTH_SHORT).show()

            startActivity(intent)

            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        val fileA = File("/data/data/com.eraypeker.videohizlandirici")
        fileA.deleteRecursively()
    }
}